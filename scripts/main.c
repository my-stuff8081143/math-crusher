#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include "../headers/draw.h" // This header already includes ncurses.h
#include "../headers/input.h"
#include "../headers/math.h"

int main(int argc, char **argv)
{
   setlocale(LC_CTYPE, "");
   initscr();
   curs_set(0);
	noecho();

   if (argc > 1)
   {      
      if (strcmp(argv[1], "--help") == 0) {
         move((LINES /2) -10, (COLS /2) -6);
         printw("LIST OF ARGS");

         move((LINES /2) -8, (COLS /2) -24);
         printw("--fg=black: sets the foreground color to black");
         move((LINES /2) -7, (COLS /2) -24);
         printw("--fg=red: sets the foreground color to red");
         move((LINES /2) -6, (COLS /2) -24);
         printw("--fg=green: sets the foreground color to green");
         move((LINES /2) -5, (COLS /2) -24);
         printw("--fg=yellow: sets the foreground color to yellow");
         move((LINES /2) -4, (COLS /2) -24);
         printw("--fg=blue: sets the foreground color to blue");
         move((LINES /2) -3, (COLS /2) -24);
         printw("--fg=magenta: sets the foreground color to magenta");
         move((LINES /2) -2, (COLS /2) -24);
         printw("--fg=cyan: sets the foreground color to cyan");
         move((LINES /2) -1, (COLS /2) -24);
         printw("--fg=white: sets the foreground color to white");

         move((LINES /2) +1, (COLS /2) -24);
         printw("--bg=black: sets the background color to black");
         move((LINES /2) +2, (COLS /2) -24);
         printw("--bg=red: sets the background color to red");
         move((LINES /2) +3, (COLS /2) -24);
         printw("--bg=green: sets the background color to green");
         move((LINES /2) +4, (COLS /2) -24);
         printw("--bg=yellow: sets the background color to yellow");
         move((LINES /2) +5, (COLS /2) -24);
         printw("--bg=blue: sets the background color to blue");
         move((LINES /2) +6, (COLS /2) -24);
         printw("--bg=magenta: sets the background color to magenta");
         move((LINES /2) +7, (COLS /2) -24);
         printw("--bg=cyan: sets the background color to cyan");
         move((LINES /2) +8, (COLS /2) -24);
         printw("--bg=white: sets the background color to white");

         for (short i = -11; i < 10; i++) {
            move((LINES /2) +i, (COLS /2) -26);
            printw("▎");
         }

         for (short i = -11; i < 10; i++) {
            move((LINES /2) +i, (COLS /2) +28);
            printw("▎");
         }

         for (short i = -25; i < 27; i++) {
            move((LINES /2) -12, (COLS /2) +i);
            printw("▁");
         }

         for (short i = -25; i < 27; i++) {
            move((LINES /2) +9, (COLS /2) +i);
            printw("▁");
         }

         getch();
         endwin();
         exit(0);
      }

      else {
         start_color();
         int fgcolor = COLOR_WHITE;
         int bgcolor = COLOR_BLACK;

         for (int i = 1; i < argc; i++) {
            if (strstr(argv[i], "--fg=black") != NULL) {
               fgcolor = COLOR_BLACK;
               break;
            }

            else if (strstr(argv[i], "--fg=red") != NULL) {
               fgcolor = COLOR_RED;
               break;
            }

            else if (strstr(argv[i], "--fg=green") != NULL) {
               fgcolor = COLOR_GREEN;
               break;
            }

            else if (strstr(argv[i], "--fg=yellow") != NULL) {
               fgcolor = COLOR_YELLOW;
               break;
            }

            else if (strstr(argv[i], "--fg=blue") != NULL) {
               fgcolor = COLOR_BLUE;
               break;
            }

            else if (strstr(argv[i], "--fg=magenta") != NULL) {
               fgcolor = COLOR_MAGENTA;
               break;
            }
            
            else if (strstr(argv[i], "--fg=cyan") != NULL) {
               fgcolor = COLOR_CYAN;
               break;
            }

            else if (strstr(argv[i], "--fg=white") != NULL) {
               fgcolor = COLOR_WHITE;
               break;
            }
         }

         for (int i = 1; i < argc; i++) {
            if (strstr(argv[i], "--bg=black") != NULL) {
               bgcolor = COLOR_BLACK;
               break;
            }

            else if (strstr(argv[i], "--bg=red") != NULL) {
               bgcolor = COLOR_RED;
               break;
            }

            else if (strstr(argv[i], "--bg=green") != NULL) {
               bgcolor = COLOR_GREEN;
               break;
            }

            else if (strstr(argv[i], "--bg=yellow") != NULL) {
               bgcolor = COLOR_YELLOW;
               break;
            }
         
            else if (strstr(argv[i], "--bg=blue") != NULL) {
               bgcolor = COLOR_BLUE;
               break;
            }

            else if (strstr(argv[i], "--bg=magenta") != NULL) {
               bgcolor = COLOR_MAGENTA;
               break;
            }

            else if (strstr(argv[i], "--bg=cyan") != NULL) {
               bgcolor = COLOR_CYAN;
               break;
            } 

            else if (strstr(argv[i], "--bg=white") != NULL) {
               bgcolor = COLOR_WHITE;
               break;
            }
         }

         init_pair(1, fgcolor, bgcolor);
         wbkgd(stdscr, COLOR_PAIR(1));
      }
   }

	// The only purpose of these vars are to be something for the pointers to point to
	short num = 13;
	short numtwo = -16;

 	// These pointers are used to check the position in which input should be printed in the equation table
	short *posy = &num;
	short *posx = &numtwo;

	char num_one_array[24]; // This holds the user's input in the 1st line of the equation table
	char operator; // This is the operator, gets inputted at the 2nd line of the equation table
	char num_two_array[24]; // This holds the user's input in the 3rd line of the equation table

	draw_everything();

	while(1 > 0)
	{
		for(short i = 0; i < 24; i++) // In this loop, the 1st array gets assigned
		{
	   	char input = getch();
			check_quit(input);

			num_one_array[i] = input;
			if (input != 8 && input != 127) {
            input_at_table(input, posy, posx);
         }
   		blink_nums(input);

			if(input == 8 || input == 127) // Erases input
			{
				if(i > 0)
					i -= 1;

				num_one_array[i] = ' ';
		   	erase_input(posy, posx);				

				if(i > 0)
					i -= 1;
			}

			if(input == '\n')
	   		i = 25; // Breaks the loop
		}

		for(short i = 0; i < 10; i++) // In this loop, the operator gets assigned
		{
			char input = getch();
   		check_quit(input);

         if (input != 8 && input != 127) {
   		   input_at_table(input, posy, posx);
         }
   		blink_nums(input);

			if(input != '\n')
				operator = input;

			if(input == 8 || input == 127) // Erases input
			{
				operator = ' ';
		   	erase_input(posy, posx);				
				if(i > 0)
					i -= 1;
			}

			if(input == '\n')
	   		i = 11;
		}

		for(short i = 0; i < 24; i++) // In this loop, the 2nd array gets assigned
   	{
   		char input = getch();
			check_quit(input);

			num_two_array[i] = input;
   		blink_nums(input);
         if (input != 8 && input != 127) {
			   input_at_table(input, posy, posx);
         }

			if(input == 8 || input == 127) // Erases input
			{				
				if(i > 0)
					i -= 1;

				num_two_array[i] = ' ';
		   	erase_input(posy, posx);				

				if(i > 0)
					i -= 1;
			}

	   	if(input == '\n')
				i = 25; // Breaks the loop		
		}

		double num_one;
		double num_two;

		num_one = atof(num_one_array);
		num_two = atof(num_two_array);

		if(operator == '+')
			sum(num_one, num_two);

		else if(operator == '-')
			minus(num_one, num_two);

		else if(operator == '*')
			mult(num_one, num_two);

		else if(operator == '/')
			divis(num_one, num_two);

		else // If operator is not valid
		{
			erase_table_contents();
         move(MID_YPOS -12, MID_XPOS -16);
			printw("OPERATOR IS NOT VALID!!!");
		}

		getch();
		erase_table_contents();

		//Resets the position values, so that the program can loop correctly
		*posy = 13;
		*posx = -16;
	}

   endwin();
   return 0;
}
